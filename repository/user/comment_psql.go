package Repository

import (
	"database/sql"
	"fmt"

	"bitbucket.org/gourav_261999/blogging-app-golang/models"
)

type CommentRepository struct {
	DB *sql.DB
}

func (cr CommentRepository) InsertComment(comment *models.Comment, authorId int, articleId int) error {
	comment.AuthorId = authorId
	statement := "insert into comments (authorid, articleid, body, createdat, updatedAt) values ($1, $2, $3, $4, $5) returning commentid;"
	err := cr.DB.QueryRow(statement, authorId, articleId, comment.Body, comment.CreatedAt, comment.UpdatedAt).Scan(&comment.Commentid)
	return err
}

func (cr CommentRepository) GetCommentsFromArticleId(comments *models.CommentsValidator, articleId int) error {
	statement := "select commentid, authorid, body, createdat, updatedat from comments where articleid = ($1) order by createdat desc;"
	var rows *sql.Rows
	var err error
	rows, err = cr.DB.Query(statement, articleId)
	if err != nil {
		return err
	}
	fmt.Println("here")
	for rows.Next() {
		var comment models.Comment
		err = rows.Scan(&comment.Commentid, &comment.AuthorId, &comment.Body, &comment.CreatedAt, &comment.UpdatedAt)
		if err != nil {
			return err
		}
		comments.CommentList = append(comments.CommentList, comment)
	}
	return nil

}

func (cr CommentRepository) DeleteComment(articleId, commentId int) error {
	statement := "delete from comments where articleid = ($1) and commentid = ($2);"
	_, err := cr.DB.Exec(statement, articleId, commentId)
	if err != nil {
		return err
	}
	return nil
}

package Repository

import (
	"database/sql"

	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"
)

type UserRepository struct {
	DB *sql.DB
}

func (u UserRepository) InsertUser(user *models.User) error {
	statement := "insert into users (email, password, username) values($1, $2, $3) RETURNING id;"
	err := u.DB.QueryRow(statement, user.Email, user.Password, user.UserName).Scan(&user.ID)
	user.Password = ""
	return err
}

func (u UserRepository) QueryUserFromEmail(user *models.User, email string) error {
	user.Email = email

	statement := "select * from users where email=$1"

	row := u.DB.QueryRow(statement, user.Email)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.UserName, &user.Bio, &user.Image, &user.Url)
	if err != nil {
		return err
	}
	return nil
}

func (u UserRepository) QueryUserFromUsername(user *models.User, username string) error {
	user.UserName = username
	statement := "select * from users where username=$1"

	row := u.DB.QueryRow(statement, user.UserName)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.UserName, &user.Bio, &user.Image, &user.Url)
	if err != nil {
		return err
	}
	return nil
}

func (u UserRepository) UpdateUser(user *models.User, id int) error {
	statement := "update users set username = ($1), email = ($2), password = ($3), bio = ($4), image = ($5) WHERE ID = ($6);"
	_, err := u.DB.Exec(statement, user.UserName, user.Email, user.Password, user.Bio, user.Image, id)
	return err
}

func (u UserRepository) GetUserFromID(user *models.User, id int) error {
	user.ID = id
	statement := "select * from users where id=$1"

	row := u.DB.QueryRow(statement, user.ID)
	err := row.Scan(&user.ID, &user.Email, &user.Password, &user.UserName, &user.Bio, &user.Image, &user.Url)
	if err != nil {
		return err
	}
	return nil
}

func (u UserRepository) GetProfileFromID(profile *models.Profile, id int, viewerId int) error {
	var user models.User
	err := u.GetUserFromID(&user, id)
	//spew.Dump(user)
	if err != nil {
		return err
	}
	statement := "select exists(select 1 from followers where leaderid=($1) and followerid =($2))"
	utils.UserToProfile(&user, profile)
	err = u.DB.QueryRow(statement, id, viewerId).Scan(&profile.Following)
	if err != nil {
		return err
	}
	return nil

}

func (u UserRepository) InsertFollower(leaderid, followerid int) error {
	statement := "insert into followers (leaderid, followerid) values ($1, $2);"
	_, err := u.DB.Query(statement, leaderid, followerid)
	if err != nil {
		return err
	}
	return nil
}

func (u UserRepository) DeleteFollower(leaderid, followerid int) error {
	statement := "delete from followers where leaderid = ($1) and followerid = ($2);"
	_, err := u.DB.Query(statement, leaderid, followerid)
	if err != nil {
		return err
	}
	return nil
}

func (u UserRepository) CheckEmailandUsername(email, username string) models.CommonError {
	err := models.CommonError{}
	var emailExists bool
	var usernameExists bool
	err.Errors = make(map[string]interface{})
	statement := "select exists(select 1 from users where email = ($1))"
	u.DB.QueryRow(statement, email).Scan(&emailExists)
	if emailExists {
		err.Errors["email"] = "is already taken"
	}
	statement = "select exists(select 1 from users where username = ($1))"
	u.DB.QueryRow(statement, username).Scan(&usernameExists)
	if usernameExists {
		err.Errors["username"] = "is already taken"
	}
	return err

}
func (u UserRepository) CheckUserName(username string) models.CommonError {
	err := models.CommonError{}
	var emailExists bool
	err.Errors = make(map[string]interface{})
	statement := "select exists(select 1 from users where username = ($1))"
	u.DB.QueryRow(statement, username).Scan(&emailExists)
	if emailExists {
		err.Errors["username"] = "is already taken"
	}
	return err

}

func (u UserRepository) GetIdFromEmail(email string) (int, error) {
	var id int
	statement := "select id from users where email = ($1)"
	err := u.DB.QueryRow(statement, email).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

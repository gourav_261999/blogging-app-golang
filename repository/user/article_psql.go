package Repository

import (
	"database/sql"
	"fmt"

	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"

	"github.com/lib/pq"
)

type ArticleRepository struct {
	DB *sql.DB
}

func (a ArticleRepository) InsertArticle(article *models.Article, id int) error {
	statement := "insert into articles ( authorid, title, body, description, createdat, tags, slug) values ($1, $2, $3, $4, $5, $6, $7) returning articleid"
	err := a.DB.QueryRow(statement, id, article.Title, article.Body, article.Description, article.CreatedAt, pq.Array(article.Tags), article.Slug).Scan(&article.ArticleID)
	return err
}

func (a ArticleRepository) GetTags(tags *models.TagsValidator) error {
	statement := "select distinct unnest(tags) from articles;"
	rows, err := a.DB.Query(statement)
	if err != nil {
		return err
	}
	for rows.Next() {
		var tag string
		err = rows.Scan(&tag)
		if err != nil {
			return err
		}
		tags.Taglist = append(tags.Taglist, tag)
	}
	return nil
}

func (a ArticleRepository) GetGLobalFeed(articles *models.ArticlesValidator, limit int, offset int) error {
	statement := "select * from articles order by createdat desc limit $1 offset $2;"
	var rows *sql.Rows
	var err error
	rows, err = a.DB.Query(statement, limit, offset)
	if err != nil {
		return err
	}
	err = utils.FormatArticles(rows, articles)

	return err

}

func (a ArticleRepository) GetArticleFromSlug(article *models.Article, slug string) error {
	article.Slug = slug
	statement := "select * from articles where slug=$1"

	row := a.DB.QueryRow(statement, article.Slug)
	err := row.Scan(&article.ArticleID, &article.AuthorID, &article.Title, &article.Body, &article.Description, &article.CreatedAt, pq.Array(&article.Tags), &article.Slug)
	if err != nil {
		return err
	}
	return nil

}

func (a ArticleRepository) GetArticleFromId(userRepo *UserRepository, article *models.Article, articleId int, userId int) error {

	article.ArticleID = articleId
	statement := "select * from articles where articleid=$1"

	row := a.DB.QueryRow(statement, article.ArticleID)
	err := row.Scan(&article.ArticleID, &article.AuthorID, &article.Title, &article.Body, &article.Description, &article.CreatedAt, pq.Array(&article.Tags), &article.Slug)
	if err != nil {
		return err
	}
	err = userRepo.GetProfileFromID(&article.Author, article.AuthorID, userId)
	if err != nil {
		return err
	}
	checkStatement := "select exists(select 1 from likes where articleid=($1) and userid =($2))"
	err = a.DB.QueryRow(checkStatement, articleId, userId).Scan(&article.Favorited)
	if err != nil {
		return err
	}
	countLikes := "select count(*) from likes where articleid = ($1)"
	err = a.DB.QueryRow(countLikes, articleId).Scan(&article.FavoritesCount)
	if err != nil {
		return err
	}
	return nil

}

func (a ArticleRepository) UpdateArticle(article *models.Article, id int) error {
	statement := "update articles set title = ($1), body = ($2), description = ($3), tags = ($4) where articleid = ($5);"
	_, err := a.DB.Exec(statement, article.Title, article.Body, article.Description, pq.Array(article.Tags), id)
	return err
}

func (a ArticleRepository) DeleteArticle(id int) error {
	statement := "delete from articles where articleid = ($1)"
	_, err := a.DB.Exec(statement, id)
	return err
}

func (a ArticleRepository) GetArticlesFromTag(articles *models.ArticlesValidator, tag string, limit int, offset int) error {
	statement := "select * from articles where ($1) = ANY(tags) order by createdat limit ($2) offset ($3);"
	rows, err := a.DB.Query(statement, tag, limit, offset)
	if err != nil {
		return err
	}
	err = utils.FormatArticles(rows, articles)
	return err
}

func (a ArticleRepository) GetIdFromSlug(slug string) (int, error) {
	statement := "select articleid from articles where slug = ($1);"
	var id int
	err := a.DB.QueryRow(statement, slug).Scan(&id)
	return id, err
}

func (a ArticleRepository) AddLike(articleId, userId int) error {
	var id int
	statement := "insert into likes (articleid, userid) values ($1, $2) returning likeid;"
	err := a.DB.QueryRow(statement, articleId, userId).Scan(&id)
	return err
}

func (a ArticleRepository) DeleteLike(articleId, userId int) error {
	statement := "delete from likes where articleid = ($1) and userid = ($2)"
	_, err := a.DB.Exec(statement, articleId, userId)
	return err
}

func (a ArticleRepository) GetPostsFromId(articles *models.ArticlesValidator, userId int, limit int, offset int) error {
	statement := "select * from articles where authorid=($1) order by createdat desc limit ($2) offset ($3) ;"
	rows, err := a.DB.Query(statement, userId, limit, offset)
	if err != nil {
		return err
	}
	err = utils.FormatArticles(rows, articles)
	return err
}

func (a ArticleRepository) GetLikedPostsFromUsername(articles *models.ArticlesValidator, userId int, limit int, offset int) error {
	statement := "select * from articles where articles.articleid in (select likes.articleid from likes where likes.userid = ($1)) order by createdat desc limit ($2) offset ($3)"
	fmt.Println(userId, limit, offset)
	rows, err := a.DB.Query(statement, userId, limit, offset)
	if err != nil {
		return err
	}
	err = utils.FormatArticles(rows, articles)
	return err

}

func (a ArticleRepository) GetFeed(articles *models.ArticlesValidator, viewerId int, limit int, offset int) error {
	statement := "select * from articles where articles.articleid in (select articles.articleid where articles.authorid in (select followers.leaderid from followers where followers.followerid = ($1))) order by articles.createdat desc limit ($2) offset ($3)"
	rows, err := a.DB.Query(statement, viewerId, limit, offset)
	if err != nil {
		return err
	}
	err = utils.FormatArticles(rows, articles)
	return err
}

package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/gourav_261999/blogging-app-golang/middleware"
	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"
	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
)

// PostArticle godoc
// @Summary takes article title, description, body and tags
// @Description stores the post in the database
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200
// @Failure 401
// @Router /articles/ [post]
func (c Controller) PostArticle(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	var articleValidator models.ArticleValidator
	var article models.Article
	var error models.Error
	var User models.User
	json.NewDecoder(r.Body).Decode(&articleValidator)
	verror := utils.Check(articleValidator)
	if len(verror.Errors) != 0 {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
		return
	}
	article = articleValidator.Article
	article.CreatedAt = utils.GetTime()
	article.Slug = utils.GetSlug(article.Title)

	email := authInfo.Email
	c.UserRepository.QueryUserFromEmail(&User, email)

	err := c.ArticleRepository.InsertArticle(&article, User.ID)
	if err != nil {
		fmt.Println(err)
		error.Message = "Server Error"
		utils.RespondWithError(w, http.StatusInternalServerError, []models.Error{error})
	}
	utils.ResponseJSON(w, models.ArticleValidator{Article: article})

}

// Tags godoc
// @Summary returns all the tags of the articles
// @Accept json
// @Description stores the post in the database
// @Produce json
// @Success 200
// @Router /tags [get]
func (c Controller) Tags(w http.ResponseWriter, r *http.Request) {
	var Tags models.TagsValidator
	c.ArticleRepository.GetTags(&Tags)
	utils.ResponseJSON(w, Tags)
}

// UpdateArticle godoc
// @Summary takes slug and returns/updates/deletes the article
// @Description takes slug and returns/updates/deletes the article
// @Produce json
// @Security ApiKeyAuth
// @Param slug path string true "article slug"
// @Success 200
// @Failure 400
// @Router /articles/{slug} [get]
func (c Controller) ArticleHanlder(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	fmt.Println("inside get article")
	var article models.Article
	var profile models.Profile
	var articleValidator models.ArticleValidator
	var updatedArticle models.ArticleValidator

	params := mux.Vars(r)
	slug := params["slug"]

	err := c.ArticleRepository.GetArticleFromSlug(&article, slug)
	if err != nil {
		if err == sql.ErrNoRows {
			utils.RespondWithError(w, http.StatusNotFound, "404 not found")
			return
		}
		log.Fatal(err)
	}
	viewerId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	err = c.UserRepository.GetProfileFromID(&profile, article.AuthorID, viewerId)

	if err != nil {
		log.Fatal(err)
	}
	if r.Method == "DELETE" {
		err = c.ArticleRepository.DeleteArticle(article.ArticleID)
		if err != nil {
			log.Fatal(err)
		}

		return
	}
	if r.Method == "PUT" {
		json.NewDecoder(r.Body).Decode(&updatedArticle)
		err = c.ArticleRepository.UpdateArticle(&updatedArticle.Article, article.ArticleID)
		if err != nil {
			log.Fatal(err)
		}
		utils.ResponseJSON(w, updatedArticle)
		return
	}
	article.Author = profile
	articleValidator.Article = article
	utils.ResponseJSON(w, articleValidator)
}

// SortArticles godoc
// @Summary takes tag, limit, offset as params and returns list of articles
// @Description takes tag, limit, offset as params and returns list of articles
// @Produce json
// @Param tag path string true "article tag"
// @Success 200
// @Failure 400
// @Router /articles/{tag} [get]
func (c Controller) SortArticlesByTags(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	fmt.Println("inside sort articles by tag")
	params := mux.Vars(r)
	tag := params["tag"]
	limit, _ := strconv.Atoi(params["limit"])
	offset, _ := strconv.Atoi(params["offset"])
	userId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	var articles models.ArticlesValidator
	c.ArticleRepository.GetArticlesFromTag(&articles, tag, limit, offset)
	for i := 0; i < articles.ArticlesCount; i++ {
		c.ArticleRepository.GetArticleFromId(c.UserRepository, &articles.ArticleList[i], articles.ArticleList[i].ArticleID, userId)
	}

	utils.ResponseJSON(w, articles)
}

// Like godoc
// @Summary likes an article
// @Description extracts userid from request and slug from the url and likes the comment
// @Produce json
// @Param slug path string true "article tag"
// @Security ApiKeyAuth
// @Success 200
// @Failure 400
// @Router /articles/{slug}/favorite [post]
func (c Controller) Like(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	var articleValidator models.ArticleValidator
	slug := mux.Vars(r)["slug"]
	articleId, err := c.ArticleRepository.GetIdFromSlug(slug)
	if err != nil {
		if err == sql.ErrNoRows {
			utils.RespondWithError(w, http.StatusNotFound, "Not Found")
		} else {
			log.Fatal(err)
		}
		return
	}
	userId, err := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	c.ArticleRepository.AddLike(articleId, userId)
	c.ArticleRepository.GetArticleFromId(c.UserRepository, &articleValidator.Article, articleId, userId)
	if err != nil {
		log.Fatal(err)
		return
	}
	utils.ResponseJSON(w, articleValidator)

}

// DisLike godoc
// @Summary deletes like from the article if you have already liked it
// @Description extracts userid from request and slug from the url and dislike likes the comment if you have already liked it
// @Produce json
// @Param slug path string true "article tag"
// @Security ApiKeyAuth
// @Success 200
// @Failure 400
// @Router /articles/{slug}/favorite [delete]
func (c Controller) DisLike(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	spew.Dump(authInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, utils.Unauthorized())
		return
	}
	var articleValidator models.ArticleValidator
	slug := mux.Vars(r)["slug"]
	articleId, err := c.ArticleRepository.GetIdFromSlug(slug)
	if err != nil {
		if err == sql.ErrNoRows {
			utils.RespondWithError(w, http.StatusNotFound, "Not Found")
		} else {
			log.Fatal(err)
		}
		return
	}
	userId, err := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	c.ArticleRepository.DeleteLike(articleId, userId)
	err = c.ArticleRepository.GetArticleFromId(c.UserRepository, &articleValidator.Article, articleId, userId)
	if err != nil {
		log.Fatal(err)
		return
	}
	utils.ResponseJSON(w, articleValidator)
}

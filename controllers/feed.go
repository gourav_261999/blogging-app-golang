package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/gourav_261999/blogging-app-golang/middleware"
	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"
	"github.com/gorilla/mux"
)

// GlobalFeed godoc
// @Summary returns all the articles with given offset and limit
// @Description returns all the articles with given offset and limit
// @Produce json
// @Param offset query int true "Offset"
// @Param limit query int true "Limit"
// @Success 200 
// @Failure 500
// @Router /articles [get]
func (c Controller) GlobalFeed(w http.ResponseWriter, r *http.Request) {
	fmt.Println("inside global feed")
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	params := mux.Vars(r)
	limit, _ := strconv.Atoi(params["limit"])
	offset, _ := strconv.Atoi(params["offset"])
	var articles models.ArticlesValidator
	err := c.ArticleRepository.GetGLobalFeed(&articles, limit, offset)
	userId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i := 0; i < articles.ArticlesCount; i++ {
		c.ArticleRepository.GetArticleFromId(c.UserRepository, &articles.ArticleList[i], articles.ArticleList[i].ArticleID, userId)
	}

	utils.ResponseJSON(w, articles)

}

// FeedHandler godoc
// @Summary returns all the articles from the users whom the viewer follows with given offset and limit
// @Description returns all the articles from the users whom the viewer follows with given offset and limit
// @Produce json
// @Param offset query int true "Offset"
// @Param limit query int true "Limit"
// @Security ApiKeyAuth
// @Success 200
// @Failure 400
// @Router /articles [get]
func (c Controller) FeedHanlder(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	params := mux.Vars(r)
	limit, _ := strconv.Atoi(params["limit"])
	offset, _ := strconv.Atoi(params["offset"])
	viewerId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	var articles models.ArticlesValidator
	c.ArticleRepository.GetFeed(&articles, viewerId, limit, offset)
	for i := 0; i < articles.ArticlesCount; i++ {
		c.ArticleRepository.GetArticleFromId(c.UserRepository, &articles.ArticleList[i], articles.ArticleList[i].ArticleID, viewerId)
	}
	utils.ResponseJSON(w, articles)

}

// GetPostsFromUsername godoc
// @Summary returns all article of a particular username
// @Description returns all article of a particular username
// @Produce json
// @Param offset query int true "Offset"
// @Param limit query int true "Limit"
// @Success 200
// @Failure 400
// @Router /articles [get]
func (c Controller) GetPostsFromUsername(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	fmt.Println("get posts from username")
	params := mux.Vars(r)
	username := params["username"]
	limit, _ := strconv.Atoi(params["limit"])
	offset, _ := strconv.Atoi(params["offset"])
	viewerId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	fmt.Println(username, limit, offset)
	var user models.User
	var articles models.ArticlesValidator
	c.UserRepository.QueryUserFromUsername(&user, username)
	c.ArticleRepository.GetPostsFromId(&articles, user.ID, limit, offset)
	for i := 0; i < articles.ArticlesCount; i++ {
		c.ArticleRepository.GetArticleFromId(c.UserRepository, &articles.ArticleList[i], articles.ArticleList[i].ArticleID, viewerId)
	}
	utils.ResponseJSON(w, articles)
}

// GetPostsFromUsername godoc
// @Summary returns all articles liked by a username
// @Description returns all articles liked by a username
// @Produce json
// @Param offset query int true "Offset"
// @Param limit query int true "Limit"
// @Success 200
// @Failure 400
// @Router /articles [get]
func (c Controller) GetLikedPostsFromUsername(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	fmt.Println("inside like posts")
	params := mux.Vars(r)
	username := params["username"]
	limit, _ := strconv.Atoi(params["limit"])
	offset, _ := strconv.Atoi(params["offset"])
	viewerId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	var user models.User
	var articles models.ArticlesValidator
	c.UserRepository.QueryUserFromUsername(&user, username)
	err := c.ArticleRepository.GetLikedPostsFromUsername(&articles, user.ID, limit, offset)
	if err != nil {
		log.Fatal(err)
		return
	}
	for i := 0; i < articles.ArticlesCount; i++ {
		c.ArticleRepository.GetArticleFromId(c.UserRepository, &articles.ArticleList[i], articles.ArticleList[i].ArticleID, viewerId)
	}
	utils.ResponseJSON(w, articles)

}

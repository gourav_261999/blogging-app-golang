package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/gourav_261999/blogging-app-golang/middleware"
	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

// SignUp user godoc
// @Summary Signup
// @Description takes user information like username, email, password and create an account
// @Accept json
// @Produce json
// @Success 200
// @Failure 422
// @Router /users [post]
func (c Controller) Signup(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	var userValidator models.UserValidator
	var user models.User
	var error models.Error
	json.NewDecoder(r.Body).Decode(&userValidator)

	verror := utils.Check(userValidator)
	user = userValidator.User
	if len(verror.Errors) != 0 {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
		return
	}
	verror = c.UserRepository.CheckEmailandUsername(user.Email, user.UserName)
	if len(verror.Errors) != 0 {
		fmt.Println("here i am ")
		utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
		return
	}
	hash, err := utils.GetHashFromstring(user.Password)

	if err != nil {
		log.Fatal(err)
	}
	user.Password = hash

	err = c.UserRepository.InsertUser(&user)
	if err != nil {
		error.Message = "Server Error"
		utils.RespondWithError(w, http.StatusInternalServerError, []models.Error{error})
		return
	}
	token, err := utils.GenerateToken(user)
	if err != nil {
		error.Message = "Server Error"
		utils.RespondWithError(w, http.StatusInternalServerError, []models.Error{error})
	}
	user.Token = token
	user.Password = ""
	w.Header().Set("Content-Type", "application/json")

	utils.ResponseJSON(w, models.UserValidator{User: user})
}

// Login user godoc
// @Summary Login
// @Description takes user information like  email, password and allows the user to login
// @Accept json
// @Produce json
// @Success 200
// @Failure 422
// @Router /users/login [post]
func (c Controller) Login(w http.ResponseWriter, r *http.Request) {
	var user models.User
	var jwt models.JWT
	var verror models.CommonError
	var userValidator models.UserValidator
	json.NewDecoder(r.Body).Decode(&userValidator)
	user = userValidator.User
	user.UserName = "a"

	verror = utils.Check(user)
	if len(verror.Errors) != 0 {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
		return
	}
	password := user.Password

	err := c.UserRepository.QueryUserFromEmail(&user, user.Email)

	if err != nil {
		if err == sql.ErrNoRows {
			verror.Errors["email or password"] = "is invalild"
			utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
			return
		} else {
			log.Fatal(err)
		}
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

	if err != nil {
		verror.Errors["email or password"] = "is invalild"
		utils.RespondWithError(w, http.StatusUnprocessableEntity, verror)
		return
	}

	token, err := utils.GenerateToken(user)
	user.Token = token

	if err != nil {
		log.Fatal(err)
	}

	w.WriteHeader(http.StatusOK)
	jwt.Token = token
	user.Password = ""

	utils.ResponseJSON(w, models.UserValidator{User: user})

}

// Settings user godoc
// @Summary Login
// @Description allows user to update user information for put method and returns updated information
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200
// @Failure 422
// @Router /user [post]
func (c Controller) Settings(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, utils.Unauthorized())
		return
	}
	w.Header().Set("Content-Type", "application/json")

	var inputUserValidator models.UserValidator
	var existingUser models.User
	var inputUser models.User
	json.NewDecoder(r.Body).Decode(&inputUserValidator)

	inputUser = inputUserValidator.User
	email := authInfo.Email
	c.UserRepository.QueryUserFromEmail(&existingUser, email)

	if inputUser.Email == "" {
		inputUser.Email = existingUser.Email
	}
	if inputUser.Password != "" {
		hash, err := utils.GetHashFromstring(inputUser.Password)
		if err != nil {
			log.Fatal(err)
		}
		inputUser.Password = hash
	} else {
		inputUser.Password = existingUser.Password
	}
	if inputUser.Image == "" {
		inputUser.Image = existingUser.Image
	}
	if inputUser.UserName == "" {
		inputUser.UserName = existingUser.UserName
	}
	if inputUser.Bio == "" {
		inputUser.Bio = existingUser.Bio
	}

	err := c.UserRepository.UpdateUser(&inputUser, existingUser.ID)
	if err != nil {
		log.Fatal(err)
		return
	}
	inputUser.Password = ""
	fmt.Println("User updated successfully")
	utils.ResponseJSON(w, models.UserValidator{User: inputUser})
}

// Profiles godoc
// @Summary returns user profile
// @Description returns the user profile of the user provided in the parameters
// @Accept json
// @Produce json
// @Param username path string true "username"
// @Success 200
// @Failure 500
// @Router /profiles/{username} [get]
func (c Controller) Profiles(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	username := mux.Vars(r)["username"]
	followerId, err := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	var leader models.User
	var profile models.Profile
	c.UserRepository.QueryUserFromUsername(&leader, username)
	c.UserRepository.GetProfileFromID(&profile, leader.ID, followerId)
	utils.ResponseJSON(w, models.ProfileValidator{Profile: profile})
}

// Follow godoc
// @Summary returns user profile
// @Description returns the user profile of the user provided in the parameters
// @Produce json
// @Security ApiKeyAuth
// @Param username path string true "username"
// @Success 200
// @Failure 401
// @Router /profiles/{username}/follow [post]
func (c Controller) Follow(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, utils.Unauthorized())
		return
	}
	username := mux.Vars(r)["username"]
	followerId, err := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	var leader models.User
	var profile models.Profile
	c.UserRepository.QueryUserFromUsername(&leader, username)
	c.UserRepository.InsertFollower(leader.ID, followerId)
	c.UserRepository.GetProfileFromID(&profile, leader.ID, followerId)
	utils.ResponseJSON(w, models.ProfileValidator{Profile: profile})

}

// UnFollow godoc
// @Summary returns user profile
// @Description returns the user profile of the user provided in the parameters
// @Produce json
// @Security ApiKeyAuth
// @Param username path string true "username"
// @Success 200
// @Failure 401
// @Router /profiles/{username}/follow [delete]
func (c Controller) UnFollow(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, utils.Unauthorized())
		return
	}
	username := mux.Vars(r)["username"]
	followerId, err := c.UserRepository.GetIdFromEmail(authInfo.Email)
	if err != nil {
		log.Fatal(err)
		return
	}
	var leader models.User
	var profile models.Profile
	c.UserRepository.QueryUserFromUsername(&leader, username)
	c.UserRepository.DeleteFollower(leader.ID, followerId)
	c.UserRepository.GetProfileFromID(&profile, leader.ID, followerId)
	utils.ResponseJSON(w, models.ProfileValidator{Profile: profile})
}

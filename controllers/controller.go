package controllers

import (
	"database/sql"
	"log"

	_ "bitbucket.org/gourav_261999/blogging-app-golang/docs"
	"bitbucket.org/gourav_261999/blogging-app-golang/middleware"
	Repository "bitbucket.org/gourav_261999/blogging-app-golang/repository/user"
	"github.com/gorilla/mux"
)

type Controller struct {
	DB                *sql.DB
	Log               *log.Logger
	UserRepository    *Repository.UserRepository
	ArticleRepository *Repository.ArticleRepository
	CommentRepository *Repository.CommentRepository
}

func (c Controller) RegisterRoutes(r *mux.Router) {
	// Routes
	c.Swagger(r)
	r.HandleFunc("/users", c.Signup).Methods("POST")
	r.HandleFunc("/users/login", c.Login).Methods("POST")
	r.HandleFunc("/user", middleware.TokenVerifyMiddleWare(c.Settings)).Methods("GET", "POST", "PUT")
	r.HandleFunc("/profiles/{username}", middleware.TokenVerifyMiddleWare(c.Profiles)).Methods("GET")
	r.HandleFunc("/articles/", middleware.TokenVerifyMiddleWare(c.PostArticle)).Methods("POST")
	r.HandleFunc("/tags", c.Tags)
	r.HandleFunc("/articles", middleware.TokenVerifyMiddleWare(c.GetPostsFromUsername)).Queries("author", "{username}", "limit", "{limit}", "offset", "{offset}").Methods("GET")
	r.HandleFunc("/articles", middleware.TokenVerifyMiddleWare(c.GetLikedPostsFromUsername)).Queries("favorited", "{username}", "limit", "{limit}", "offset", "{offset}").Methods("GET")
	r.HandleFunc("/articles/feed", middleware.TokenVerifyMiddleWare(c.FeedHanlder)).Queries("limit", "{limit}", "offset", "{offset}").Methods("Get")
	r.HandleFunc("/articles", middleware.TokenVerifyMiddleWare(c.SortArticlesByTags)).Queries("tag", "{tag}", "limit", "{limit}", "offset", "{offset}")
	r.HandleFunc("/articles", middleware.TokenVerifyMiddleWare(c.GlobalFeed)).Queries("limit", "{limit}", "offset", "{offset}")
	r.HandleFunc("/articles/{slug}", middleware.TokenVerifyMiddleWare(c.ArticleHanlder)).Methods("GET", "PUT", "DELETE")
	r.HandleFunc("/articles/{slug}/comments", middleware.TokenVerifyMiddleWare(c.GetComments)).Methods("GET")
	r.HandleFunc("/articles/{slug}/comments", middleware.TokenVerifyMiddleWare(c.PostComment)).Methods("POST")
	r.HandleFunc("/articles/{slug}/comments/{id}", middleware.TokenVerifyMiddleWare(c.DeleteComment)).Methods("DELETE")
	r.HandleFunc("/articles/{slug}/favorite", middleware.TokenVerifyMiddleWare(c.Like)).Methods("POST")
	r.HandleFunc("/articles/{slug}/favorite", middleware.TokenVerifyMiddleWare(c.DisLike)).Methods("DELETE")
	r.HandleFunc("/profiles/{username}/follow", middleware.TokenVerifyMiddleWare(c.Follow)).Methods("POST")
	r.HandleFunc("/profiles/{username}/follow", middleware.TokenVerifyMiddleWare(c.UnFollow)).Methods("DELETE")

}

package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/gourav_261999/blogging-app-golang/middleware"
	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"bitbucket.org/gourav_261999/blogging-app-golang/utils"
	"github.com/gorilla/mux"
)

// GetComments godoc
// @Summary returns the comments in an article
// @Description takes slug from the url and returns all the comments in it
// @Produce json
// @Param slug path string true "article slug"
// @Success 200
// @Failure 400 
// @Router /articles/{slug}/comments [get]
func (c Controller) GetComments(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	fmt.Println("inside get comments")
	var commentsValidator models.CommentsValidator
	var article models.Article
	slug := mux.Vars(r)["slug"]
	c.ArticleRepository.GetArticleFromSlug(&article, slug)
	commentsValidator.CommentList = []models.Comment{}
	err := c.CommentRepository.GetCommentsFromArticleId(&commentsValidator, article.ArticleID)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Fatal(err)
			return
		}
	}
	userId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	for i := 0; i < len(commentsValidator.CommentList); i++ {
		c.UserRepository.GetProfileFromID(&commentsValidator.CommentList[i].Author, commentsValidator.CommentList[i].AuthorId, userId)
	}

	utils.ResponseJSON(w, commentsValidator)

}

// PostComment godoc
// @Summary Post comment in an article
// @Description posts a comment to an article
// @Produce json
// @Param slug path string true "article slug"
// @Security ApiKeyAuth
// @Success 200
// @Failure 400
// @Router /articles/{slug}/comments [post]
func (c Controller) PostComment(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	fmt.Println("inside post comment")
	var commentValidator models.CommentValidator
	var comment models.Comment
	var article models.Article
	slug := mux.Vars(r)["slug"]
	json.NewDecoder(r.Body).Decode(&commentValidator)
	comment = commentValidator.Comment
	userId, _ := c.UserRepository.GetIdFromEmail(authInfo.Email)
	comment.CreatedAt = utils.GetTime()
	comment.UpdatedAt = utils.GetTime()
	c.UserRepository.GetProfileFromID(&comment.Author, userId, userId)
	err := c.ArticleRepository.GetArticleFromSlug(&article, slug)
	if err != nil {
		utils.RespondWithError(w, http.StatusNotFound, "Not Found")
	}
	c.CommentRepository.InsertComment(&comment, userId, article.ArticleID)
	utils.ResponseJSON(w, models.CommentValidator{Comment: comment})
}

// DeleteComment godoc
// @Summary Deletes article in an article
// @Description takes slug and id in the url and deletes the comment with given commentid
// @Produce json
// @Param slug path string true "article slug"
// @Security ApiKeyAuth
// @Success 200
// @Failure 400
// @Router /articles/{slug}/comments/{id} [delete]
func (c Controller) DeleteComment(w http.ResponseWriter, r *http.Request) {
	key := middleware.ContextKey("ContextInfo")
	authInfo, _ := r.Context().Value(key).(middleware.ContextInfo)
	if !authInfo.IsAuthorized {
		utils.RespondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}
	fmt.Println("inside delete comments")
	params := mux.Vars(r)
	var article models.Article
	slug := params["slug"]
	id, _ := strconv.Atoi(params["id"])
	err := c.ArticleRepository.GetArticleFromSlug(&article, slug)
	if err != nil {
		utils.RespondWithError(w, http.StatusNotFound, "")
	}
	err = c.CommentRepository.DeleteComment(article.ArticleID, id)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Fatal(err)
			return
		}
	}
}

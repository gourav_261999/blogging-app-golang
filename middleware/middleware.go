package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type ContextInfo struct {
	IsAuthorized bool
	Email        string
}

type ContextKey string

func TokenVerifyMiddleWare(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		bearerToken := strings.Split(authHeader, " ")
		info := ContextInfo{IsAuthorized: false, Email: ""}
		key := ContextKey("ContextInfo")

		if len(bearerToken) == 2 {
			authToken := bearerToken[1]

			token, _ := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an error")
				}

				return []byte("secret"), nil
			})
			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				info.IsAuthorized = true
				info.Email, _ = claims["email"].(string)

			}
		}
		ctx := context.WithValue(r.Context(), key, info)
		next.ServeHTTP(w, r.WithContext(ctx))
	})

}

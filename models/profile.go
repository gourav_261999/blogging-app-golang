package models

type Profile struct {
	UserName  string `json:"username" validate:"omitempty"`
	Bio       string `json:"bio" validate:"omitempty"`
	Image     string `json:"image" validate:"omitempty"`
	Following bool   `json:"following" validate:"omitempty"`
}

type ProfileValidator struct {
	Profile Profile `json:"profile" validate:"omitempty"`
}

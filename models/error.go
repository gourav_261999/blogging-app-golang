package models

type Error struct {
	Message string `json:"message"`
}
type CommonError struct {
	Errors map[string]interface{} `json:"errors"`
}

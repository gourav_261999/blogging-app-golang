package models

type User struct {
	ID       int    `json:"id" validate:"omitempty"`
	UserName string `json:"username" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	Bio      string `json:"bio" validate:"omitempty"`
	Image    string `json:"image" validate:"omitempty"`
	Token    string `json:"token" validate:"omitempty"`
	Url      string `json:"url" validate:"omitempty"`
}

type UserValidator struct {
	User User `json:"user" validate:"required"`
}

package models

type Comment struct {
	Commentid int     `json:"id" validate:"omitempty"`
	CreatedAt string  `json:"createdAt" validate:"omitempty"`
	Body      string  `json:"body" validate:"required"`
	UpdatedAt string  `json:"updatedAt" validate:"omitempty"`
	Author    Profile `json:"author" validate:"omitempty"`
	AuthorId  int     `json:"authorid" validate:"omitempty"`
	//ArticleSlug string  `json:"articleSlug" validate:"omitempty"`
}

type CommentValidator struct {
	Comment `json:"comment" validate:"required"`
}

type CommentsValidator struct {
	CommentList []Comment `json:"comments" validate:"omitempty"`
}

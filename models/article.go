package models

type Article struct {
	ArticleID      int      `json:"article-id" validate:"omitempty"`
	Title          string   `json:"title" validate:"required"`
	Body           string   `json:"body" validate:"required"`
	Description    string   `json:"description" validate:"required"`
	Tags           []string `json:"tagList" validate:"omitempty"`
	AuthorID       int      `json:"authord-id" validate:"omitempty"`
	CreatedAt      string   `json:"createdAt" validate:"omitempty"`
	UpdatedAt      string   `json:"updatedAt" validate:"omitempty"`
	Slug           string   `json:"slug" validate:"omitempty"`
	Favorited      bool     `json:"favorited" validate:"omitempty"`
	FavoritesCount int      `json:"favoritesCount" validate:"omitempty"`
	Author         Profile  `json:"author" validate:"omitempty"`
}

type ArticleValidator struct {
	Article Article `json:"article" validate:"omitempty"`
}

type ArticlesValidator struct {
	ArticleList   []Article `json:"articles" validate:"omitempty"`
	ArticlesCount int       `json:"articlesCount" validate:"omitempty"`
}

type TagsValidator struct {
	Taglist []string `json:"tags" validate:"omitempty"`
}

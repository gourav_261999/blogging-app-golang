package utils

import (
	"crypto/rand"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/gourav_261999/blogging-app-golang/models"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-playground/validator"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func Check(val interface{}) models.CommonError {
	validate := validator.New()
	res := models.CommonError{}
	res.Errors = make(map[string]interface{})
	err := validate.Struct(val)
	fmt.Println(err)
	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			if e.Param() != "" {
				res.Errors[e.Field()] = fmt.Sprintf("{%v %v}", e.Tag(), e.Param())
			} else {
				res.Errors[e.Field()] = fmt.Sprintf("{key : %v}", e.Tag())
			}
		}
	}
	return res

}

func RespondWithError(w http.ResponseWriter, status int, error interface{}) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(error)

}

func ResponseJSON(w http.ResponseWriter, data interface{}) {
	json.NewEncoder(w).Encode(data)
}

func GetHashFromstring(Password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(Password), 10)
	if err != nil {
		return "", err
	}
	return string(hash), nil

}

func GenerateToken(user models.User) (string, error) {
	var err error
	secret := "secret"

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": user.Email,
		"iss":   "course",
	})

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Fatal(err)
	}
	return tokenString, nil
}

func FormatArticles(rows *sql.Rows, articles *models.ArticlesValidator) error {
	var err error
	articles.ArticlesCount = 0
	for rows.Next() {
		var article models.Article
		err = rows.Scan(&article.ArticleID, &article.AuthorID, &article.Title, &article.Body, &article.Description, &article.CreatedAt, pq.Array(&article.Tags), &article.Slug)
		if err != nil {
			return err
		}
		articles.ArticleList = append(articles.ArticleList, article)
		articles.ArticlesCount += 1
	}
	return err

}

func GetSlug(title string) string {
	RandomCrypto, _ := rand.Prime(rand.Reader, 128)
	slug := strings.ReplaceAll(title, " ", "-")
	slug += ("-" + RandomCrypto.String())
	return slug
}

func UserToProfile(user *models.User, profile *models.Profile) {
	profile.Bio = user.Bio
	profile.Image = user.Image
	profile.UserName = user.UserName
}

func GetTime() string {
	current_time := time.Now()
	s := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d.%03dZ",
		current_time.Year(), current_time.Month(), current_time.Day(),
		current_time.Hour(), current_time.Minute(), current_time.Second(), current_time.Nanosecond())
	return s

}

func Unauthorized() models.CommonError {
	var err models.CommonError
	err.Errors["user"] = "Unauthorized"
	return err
}

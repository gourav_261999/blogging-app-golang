// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag

package docs

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "Gourav Roy",
            "email": "gourav.roy@jungleegames.com"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/articles": {
            "get": {
                "description": "returns all articles liked by a username",
                "produces": [
                    "application/json"
                ],
                "summary": "returns all articles liked by a username",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "Offset",
                        "name": "offset",
                        "in": "query",
                        "required": true
                    },
                    {
                        "type": "integer",
                        "description": "Limit",
                        "name": "limit",
                        "in": "query",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/": {
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "stores the post in the database",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "takes article title, description, body and tags",
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "401": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/{slug}": {
            "get": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "takes slug and returns/updates/deletes the article",
                "produces": [
                    "application/json"
                ],
                "summary": "takes slug and returns/updates/deletes the article",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article slug",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/{slug}/comments": {
            "get": {
                "description": "takes slug from the url and returns all the comments in it",
                "produces": [
                    "application/json"
                ],
                "summary": "returns the comments in an article",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article slug",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            },
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "posts a comment to an article",
                "produces": [
                    "application/json"
                ],
                "summary": "Post comment in an article",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article slug",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/{slug}/comments/{id}": {
            "delete": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "takes slug and id in the url and deletes the comment with given commentid",
                "produces": [
                    "application/json"
                ],
                "summary": "Deletes article in an article",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article slug",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/{slug}/favorite": {
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "extracts userid from request and slug from the url and likes the comment",
                "produces": [
                    "application/json"
                ],
                "summary": "likes an article",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article tag",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            },
            "delete": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "extracts userid from request and slug from the url and dislike likes the comment if you have already liked it",
                "produces": [
                    "application/json"
                ],
                "summary": "deletes like from the article if you have already liked it",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article tag",
                        "name": "slug",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/articles/{tag}": {
            "get": {
                "description": "takes tag, limit, offset as params and returns list of articles",
                "produces": [
                    "application/json"
                ],
                "summary": "takes tag, limit, offset as params and returns list of articles",
                "parameters": [
                    {
                        "type": "string",
                        "description": "article tag",
                        "name": "tag",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": ""
                    }
                }
            }
        },
        "/profiles/{username}": {
            "get": {
                "description": "returns the user profile of the user provided in the parameters",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "returns user profile",
                "parameters": [
                    {
                        "type": "string",
                        "description": "username",
                        "name": "username",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "500": {
                        "description": ""
                    }
                }
            }
        },
        "/profiles/{username}/follow": {
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "returns the user profile of the user provided in the parameters",
                "produces": [
                    "application/json"
                ],
                "summary": "returns user profile",
                "parameters": [
                    {
                        "type": "string",
                        "description": "username",
                        "name": "username",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "401": {
                        "description": ""
                    }
                }
            },
            "delete": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "returns the user profile of the user provided in the parameters",
                "produces": [
                    "application/json"
                ],
                "summary": "returns user profile",
                "parameters": [
                    {
                        "type": "string",
                        "description": "username",
                        "name": "username",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "401": {
                        "description": ""
                    }
                }
            }
        },
        "/tags": {
            "get": {
                "description": "stores the post in the database",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "returns all the tags of the articles",
                "responses": {
                    "200": {
                        "description": ""
                    }
                }
            }
        },
        "/user": {
            "post": {
                "security": [
                    {
                        "ApiKeyAuth": []
                    }
                ],
                "description": "allows user to update user information for put method and returns updated information",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Login",
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "422": {
                        "description": ""
                    }
                }
            }
        },
        "/users": {
            "post": {
                "description": "takes user information like username, email, password and create an account",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Signup",
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "422": {
                        "description": ""
                    }
                }
            }
        },
        "/users/login": {
            "post": {
                "description": "takes user information like  email, password and allows the user to login",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Login",
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "422": {
                        "description": ""
                    }
                }
            }
        }
    },
    "securityDefinitions": {
        "ApiKeyAuth": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header"
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "1.0",
	Host:        "localhost:8000",
	BasePath:    "/",
	Schemes:     []string{},
	Title:       "Blogging App Golang",
	Description: "This is a blogging app like medium",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
